# mariadb

This is a docker image where we are running MariaDB database server in our already created CentOS 7 image. The image has a necessary variable which is `MYSQL_ROOT_PASSWORD` and we have to set it in order to create a password for our root user. 

There are other variables as well that we are using which are optional. These are:

- `MYSQL_USER` and `MYSQL_PASSWORD`. These two variables are used to create a user when we are creating the container. They both need to be set to provide a `username` and a `password` for our user. 
- `MYSQL_DATABASE`. This variable can be set on it's own and then we are simply creating a database which is assigned to nobody. If `MYSQL_USER` and `MYSQL_PASSWORD` have already been set as well then this database will be assigned to that newly created user. 
- `MARIADB_MAX_CONNECTIONS`. This is a variable for performance purposes (default value = 100) .
- `MARIADB_BUFFER_POOL_SIZE`. This is a variable to set the buffer for InnoDB engine. It is also used for performance purposes (default value = 128M).

We are configuring our database in the `/etc/cont-init.d/10-configure-mariadb` file where we are creating some necessary directories and set the permissions. 

Then we are initializing our database in the `/etc/cont-init.d/11-initialize-mariadb` file based on the environment variables that we have set and explained earlier. 

## Usage Examples

The simplest example we can provide is the one below:

    docker build -t mariadb .
    docker run -d --name mariadb -p 127.0.0.1:3306:3306 -e MYSQL_ROOT_PASSWORD=1234 mariadb 

We need to pay attention on the **-p** flag where we are publishing host's **127.0.0.1:3306** to container's **3306**. That way we can remotely connect to our database from the host to the container. That can be done with the command 

    mysql -u root -h 127.0.0.1 -p 

We are also providing and a docker-compose file with every setting set up but in order to make it work we have to create a `.env` file and store the environment variables there. The `.env` file should have the below variables that we set them accordingly on our needs.

    MYSQL_ROOT_PASSWORD=
    MYSQL_DATABASE=
    MYSQL_USER=
    MYSQL_PASSWORD=


