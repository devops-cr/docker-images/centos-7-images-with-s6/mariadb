FROM centos:7-christos

COPY etc/ /etc/

RUN groupmod -g 100 users \
 && groupadd -g 990 db_user \
 && useradd -u 990 -g 990 -d /mysql -s /bin/false db_user \
 && usermod -G users db_user \
 && yum -y install \
    MariaDB-server \
 && yum clean all \
 && rm -rf \
           /tmp/* \
           /var/lib/mysql \
 && mkdir -p \
          /var/lib/mysql \
          /var/run/mysqld \
          /mysql


ARG MARIADB_PORT=3306

ENV \
    MARIADB_MAX_CONNECTIONS=100 \
    MARIADB_BUFFER_POOL_SIZE=128M 
    
EXPOSE $MARIADB_PORT
    
